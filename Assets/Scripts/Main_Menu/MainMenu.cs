﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void LoadSinglePlayer()
    {
        Debug.Log("Single player Loading");
        //load scene
        SceneManager.LoadScene("Single_Player");
    }

    public void LoadCoopMode()
    {
        Debug.Log("Coop loading");
        SceneManager.LoadScene("Co-op_Mode");
    }
}
