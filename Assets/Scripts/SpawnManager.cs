﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private GameObject enemyShip;

    [SerializeField] private GameObject[] powerups;

    private GameManager _gameManager;

    // Start is called before the first frame update
    void Start()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        StartCoroutine(SpawnEnemyRoutine());
        StartCoroutine(PowerupSpawnRoutine());
    }

    public void StartSpawnRoutines()
    {
        StartCoroutine(SpawnEnemyRoutine());
        StartCoroutine(PowerupSpawnRoutine());
    }

    //coroutine to spawn the enemy every 5 seconds
    private IEnumerator SpawnEnemyRoutine()
    {
        while (_gameManager.gameOver == false)
        {
            Instantiate(enemyShip, new Vector3(Random.Range(-7.5f, 7.5f), 7f, 0), Quaternion.identity);
            yield return new WaitForSeconds(5f);
        }
        
    }

    IEnumerator PowerupSpawnRoutine()
    {
        while (_gameManager.gameOver == false)
        {
            int randomPowerUp = Random.Range(0, 3);
            Instantiate(powerups[randomPowerUp], new Vector3(Random.Range(-7.5f, 7.5f), 7f, 0), Quaternion.identity);
            yield return new WaitForSeconds(10f);
        }
    }
}
