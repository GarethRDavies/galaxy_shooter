﻿using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] private float _speed = 5f;
    [SerializeField] private GameObject _enemyExplosion;
    [SerializeField] private AudioClip _clip;

    private UIManager _uiManager;

    // Start is called before the first frame update
    void Start()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //move down
        transform.Translate(Vector3.down * _speed * Time.deltaTime);
        //when off the screen on the bottom
        //reapawn back on top with a new x position between the bounds of the screen
        if (transform.position.y < -7f)
        {
            float randomX = Random.Range(-7.5f, 7.5f);
            transform.position = new Vector2(randomX, 7f); 
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            if (player)
            {
                player.Damage();
            }

            Instantiate(_enemyExplosion, transform.position, Quaternion.identity);
            _uiManager.UpdateScore();
            AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position);
            Destroy(gameObject);
        }

        if (other.tag == "Laser")
        {
            Instantiate(_enemyExplosion, transform.position, Quaternion.identity);
            Destroy(other.gameObject);
            _uiManager.UpdateScore();
            AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position);
            Destroy(gameObject);
        }
    }
}
