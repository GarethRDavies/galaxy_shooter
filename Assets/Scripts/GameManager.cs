﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool isCoopMode = false;
    public bool gameOver = true;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject players;

    [SerializeField] private GameObject _pauseMenuPanel;

    private UIManager _uiManager;

    private SpawnManager _spawnManager;

    private Animator _pauseAnim;

    private void Start()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _pauseAnim = GameObject.Find("Pause_Menu_Panel").GetComponent<Animator>();
        _pauseAnim.updateMode = AnimatorUpdateMode.UnscaledTime;
    }

    //if gameover 
    //if spacekey pressed
    //spawn player
    //gameover false
    //hide title screen

    private void Update()
    {
        if (gameOver)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (isCoopMode == false)
                {
                    Instantiate(player, Vector3.zero, Quaternion.identity);
                }
                else
                {
                    Instantiate(players, Vector3.zero, Quaternion.identity);
                }
                gameOver = false;
                _uiManager.HideTitleScreen();
                _spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
                _spawnManager.StartSpawnRoutines();
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("Main_Menu");
            }
        }

        //if p key
        //pause game
        if (Input.GetKeyDown(KeyCode.P))
        {
            _pauseMenuPanel.SetActive(true);
            _pauseAnim.SetBool("isPaused", true);
            Time.timeScale = 0f;
        }
    }

    public void ResumeGame()
    {
        _pauseMenuPanel.SetActive(false);
        Time.timeScale = 1f;
    }
}
