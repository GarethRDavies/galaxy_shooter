﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour
{
    public bool canTripleShot = false;
    public bool speedBoostOn = false;
    public bool shieldsOn = false;

    [SerializeField] private GameObject _laserPrefab;
    [SerializeField] private GameObject _explosion;
    [SerializeField] private GameObject _shieldGameObject;
    [SerializeField] private GameObject[] _engines;

    private AudioSource _audioSource;

    [SerializeField] private float _fireRate = 0.25f;
    private float _nextFire = 0f;

    [SerializeField] private float _speed = 5f;
    [SerializeField] private int _health = 3;

    private UIManager _uiManager;
    private GameManager _gameManager;
    private SpawnManager _spawnManager;

    private int _hitCount = 0;

    public bool isPlayerOne = false;
    public bool isPlayerTwo = false;

    // Start is called before the first frame update
    void Start()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();

        if (_uiManager)
        {
            _uiManager.UpdateLives(_health);
        }

        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        if(_gameManager.isCoopMode == false)
        {
            transform.position = new Vector3(0f, 0f, 0f);
        }

        _spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();

        /*if (_spawnManager)
        {
            _spawnManager.StartSpawnRoutines();
        }*/

        _audioSource = GetComponent<AudioSource>();

        _hitCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlayerOne == true)
        {
            Movement();

#if UNITY_ANDROID

            if ((Input.GetKeyDown(KeyCode.Space) || CrossPlatformInputManager.GetButtonDown("Fire")) && (isPlayerOne == true))
            {
                Shoot();
            }

#else
            if ((Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) && (isPlayerOne == true))
            {
                Shoot();
            }

#endif


        }

        if (isPlayerTwo == true)
        {
            PlayerTwoMovement();
            if ((Input.GetKeyDown(KeyCode.KeypadEnter)))
            {
                Shoot();
            }
        }
    }

    private void Movement()
    {
        float horizontalInput = CrossPlatformInputManager.GetAxis("Horizontal"); //Input.GetAxis("Horizontal");
        float verticalInput = CrossPlatformInputManager.GetAxis("Vertical"); // Input.GetAxis("Vertical");

        if (speedBoostOn)
        {
            transform.Translate(new Vector3((1.5f * _speed * horizontalInput * Time.deltaTime), (1.5f * _speed * verticalInput * Time.deltaTime), transform.position.z));
        }
        else
        {
            transform.Translate(new Vector3((_speed * horizontalInput * Time.deltaTime), (_speed * verticalInput * Time.deltaTime), transform.position.z));
        }

        if (transform.position.y > 0)
        {
            transform.position = new Vector3(transform.position.x, 0f, transform.position.z);
        }
        else if (transform.position.y < -4.2f)
        {
            transform.position = new Vector3(transform.position.x, -4.2f, transform.position.z);
        }

        if (transform.position.x > 8.2)
        {
            transform.position = new Vector3(8.2f, transform.position.y, transform.position.z);
        }
        else if (transform.position.x < -8.2)
        {
            transform.position = new Vector3(-8.2f, transform.position.y, transform.position.z);
        }
    }

    private void PlayerTwoMovement()
    {
        if (speedBoostOn)
        {
            if (Input.GetKey(KeyCode.Keypad8))
            {
                transform.Translate(Vector3.up * _speed * 1.5f * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad6))
            {
                transform.Translate(Vector3.right * _speed * 1.5f * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad2))
            {
                transform.Translate(Vector3.down * _speed * 1.5f * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad4))
            {
                transform.Translate(Vector3.left * _speed * 1.5f * Time.deltaTime);
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.Keypad8))
            {
                transform.Translate(Vector3.up * _speed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad6))
            {
                transform.Translate(Vector3.right * _speed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad2))
            {
                transform.Translate(Vector3.down * _speed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad4))
            {
                transform.Translate(Vector3.left * _speed * Time.deltaTime);
            }
        }

        if (transform.position.y > 0)
        {
            transform.position = new Vector3(transform.position.x, 0f, transform.position.z);
        }
        else if (transform.position.y < -4.2f)
        {
            transform.position = new Vector3(transform.position.x, -4.2f, transform.position.z);
        }

        if (transform.position.x > 8.2)
        {
            transform.position = new Vector3(8.2f, transform.position.y, transform.position.z);
        }
        else if (transform.position.x < -8.2)
        {
            transform.position = new Vector3(-8.2f, transform.position.y, transform.position.z);
        }
    }

    private void Shoot()
    {
        if (Time.time > _nextFire)
        {
            _audioSource.Play();
            Instantiate(_laserPrefab, transform.position + new Vector3(0f, 0.97f, 0f), Quaternion.identity);
            _nextFire = Time.time + _fireRate;

            if (canTripleShot)
            {
                Instantiate(_laserPrefab, transform.position + new Vector3(0.55f, 0.06f, 0f), Quaternion.identity);
                Instantiate(_laserPrefab, transform.position + new Vector3(-0.55f, 0.06f, 0f), Quaternion.identity);
            }
  
        }
    }

    public void TripleShotPowerupOn()
    {
        canTripleShot = true;
        StartCoroutine(TripleShotPowerDownRoutine());
    }

    public void SpeedBoostOn()
    {
        speedBoostOn = true;
        StartCoroutine(SpeedBoostPowerDownRoutine());
    }

    public void ShieldsOn()
    {
        shieldsOn = true;
        _shieldGameObject.SetActive(true);
    }

    public void Damage()
    {
        if (shieldsOn)
        {
            shieldsOn = false;
            _shieldGameObject.SetActive(false);
            return;
        }

        _health -= 1;
        _uiManager.UpdateLives(_health);

        _hitCount++;

        if (_hitCount == 1)
        {
            //turn left engine failure on
            _engines[0].SetActive(true);
        }
        else if (_hitCount == 2)
        {
            //turn right engine failure on
            _engines[1].SetActive(true);
        }

        if (_health < 1)
        {
            Instantiate(_explosion, transform.position, Quaternion.identity);
            _gameManager.gameOver = true;
            _uiManager.ShowTitleScreen();
            _uiManager.CheckForBestScore();
            Destroy(gameObject);
        }
    }

    public IEnumerator TripleShotPowerDownRoutine()
    {
        yield return new WaitForSeconds(5f);
        canTripleShot = false;
    }

    public IEnumerator SpeedBoostPowerDownRoutine()
    {
        yield return new WaitForSeconds(5f);
        speedBoostOn = false;
    }
}
